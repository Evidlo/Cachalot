import tempfile
import time
from urllib import request

import pytest

from .context import cachalot


def cache_file():
    return str(tempfile.NamedTemporaryFile().name)


def plain_request():
    response = request.urlopen('http://httpbin.org/get')
    return response.read().decode('utf-8')


@cachalot.Cache(path=cache_file())
def cached_request():
    return plain_request()


@pytest.fixture(scope='session')
def cached():
    start = time.time()
    for _ in range(3):
        cached_request()
    end = time.time()
    yield end - start


@pytest.fixture(scope='session')
def uncached():
    start = time.time()
    for _ in range(3):
        plain_request()
    end = time.time()
    yield end - start


def test_speed(cached, uncached):
    # GIVEN a cached and uncached request
    # WHEN repeating the same request
    # THEN the cached request should be faster
    assert cached < uncached
