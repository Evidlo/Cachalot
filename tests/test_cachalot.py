import time

import pytest
import os
from tinydb import storages

from .context import cachalot


@pytest.fixture(params=[False, True], ids=['in_memory', 'file'], scope='session')
def cache_file(tmpdir_factory, request):
    # FIXME: hardcode MemoryStorage for now
    return None
    if request.param:
        return str(tmpdir_factory.mktemp('cache').join('.cache'))
    else:
        return None

@pytest.fixture(scope='session')
def cache(cache_file):
    return cachalot.Cache(path=cache_file,
                          size=2,
                          filesize=5000,
                          )


@pytest.fixture()
def empty_cache(cache):
    yield cache
    cache.db.truncate()


@pytest.fixture()
def full_cache(cache):
    cache.db.insert_multiple([{'key': 'test', 'time': 9999999999, 'value': '1'},
                              {'key': 'test2', 'time': 9999999999, 'value': '2'}])
    yield cache
    cache.db.truncate()


@pytest.fixture()
def expired_cache(cache):
    cache.db.insert_multiple([{'key': 'test', 'time': 1, 'value': '1'},
                              {'key': 'test2', 'time': 1, 'value': '2'}])
    yield cache
    cache.db.truncate()


class TestCache:

    def test_expiry(self, empty_cache):
        # GIVEN an empty cache
        cache = empty_cache

        # WHEN checking expiration time
        # THEN it show be roughly current time + defined expiry
        now = time.time()
        expiry = cache.expiry()
        assert int(expiry) == int(now + cache.timeout)

    def test_calculate_hash(self, cache_file):
        # GIVEN two functions, one of which is cached

        cache = cachalot.Cache(path=cache_file, size=2,
                               retry=True)

        @cache
        def mocks(arg1, arg2):
            return arg1, arg2
        mocks('test', arg2=1)

        def mocks2(arg1, arg2):
            return arg1, arg2

        # WHEN calculating their hashes using the same arguments
        calculated_hash = cache.calculate_hash(mocks)('test', arg2=1)
        calculated_hash2 = cache.calculate_hash(mocks2)('test', arg2=1)

        # THEN the hashes should be the same as in the cache, but different from each other
        assert calculated_hash == cache.db.all()[0]['key']
        assert calculated_hash != calculated_hash2

    def test_clear(self, full_cache):
        # GIVEN a cache with items
        cache = full_cache
        print(cache.db.all())
        assert len(cache) > 1

        # WHEN clearing a cache
        cache.clear()

        # THEN there should be no items left
        assert len(cache) == 0

    def test_get(self, expired_cache):
        # GIVEN a cache with expired a non-expired items
        cache = expired_cache
        cache.timeout = 86400
        cache.db.insert({'key': 'test3', 'time': 9999999999, 'value': '3'})

        # WHEN getting the results
        # THEN only the non-expired items should return a result
        assert cache.get('test') == None
        assert cache.get('test2') == None
        assert cache.get('test3') == 3

    def test_insert(self, empty_cache):
        # GIVEN an empty cache
        cache = empty_cache

        # WHEN inserting an item
        cache.insert('test', 1)

        # THEN the cache should contain only this item
        assert len(cache) == 1
        assert int(cache.db.all()[0]['value']) == 1

    def test_insert_overflow_size(self, empty_cache):
        # GIVEN an empty cache
        cache = empty_cache

        # WHEN inserting more items than the cache size
        cache.insert('test', 1)
        cache.insert('test2', 2)
        cache.insert('test3', 3)

        # THEN the oldest item should be removed
        assert len(cache) == 2
        assert int(cache.db.all()[0]['value']) == 2

    def test_insert_overflow_filesize(self, empty_cache):
        # GIVEN an empty cache
        cache = empty_cache

        # WHEN inserting more data than the cache filesize
        cache.insert('test', 'x' * 4000)  # approx. 4KB
        cache.insert('test2', 'x' * 4000)  # another 4KB

        # THEN items should be removed and the database should not exceed 5KB
        assert os.stat(cache.path).st_size < 5000

    def test_remove(self, full_cache):
        # GIVEN a cache with items
        cache = full_cache

        # WHEN removing an item
        cache.remove('test')

        # THEN the cache should only the remaining items
        assert len(cache) == 1
        assert int(cache.db.all()[0]['value']) == 2

    def test_remove_manually(self, cache_file):
        # GIVEN a cached function call
        cache = cachalot.Cache(path=cache_file, size=2,
                               retry=True)

        @cachalot.Cache(path=cache_file, size=1)
        def mocks(arg1, arg2):
            return arg1, arg2

        mocks('test', arg2=1)
        assert len(cache) == 1

        # WHEN manually removing the function call from cache
        calculated_hash = cache.calculate_hash(mocks)('test', arg2=1)
        cache.remove(calculated_hash)

        # THEN the cache should be empty
        assert len(cache) == 0

    def test_remove_oldest(self, full_cache):
        # GIVEN a cache with items
        cache = full_cache

        # WHEN removing the oldest items
        cache._remove_oldest()

        # THEN the first items inserted should be removed
        assert len(cache) == 1
        assert int(cache.db.all()[0]['value']) == 2

    def test_remove_no_timeout(self, expired_cache):
        # GIVEN a cache with expired items
        cache = expired_cache
        cache.timeout = 0

        # WHEN removing with timeout set to infinite
        cache._remove_expired()

        # THEN they should not be deleted
        assert len(cache) == 2
        assert int(cache.db.all()[0]['value']) == 1
        assert int(cache.db.all()[1]['value']) == 2

    def test_remove_expired(self, expired_cache):
        # GIVEN a cache with expired items
        cache = expired_cache
        cache.timeout = 86400

        # WHEN removing expired items
        cache._remove_expired()

        # THEN there should be none left
        assert len(cache) == 0

    def test_retry(self, cache_file):
        # GIVEN a cached function and cache with retry on

        @cachalot.Cache(path=cache_file, size=1, retry=True)
        def mocks():
            return 1

        # WHEN an empty result is cached
        mocks()
        cache = cachalot.Cache(path=cache_file, size=2,
                               retry=True)
        cache.db.update({'value': 'null'})

        # THEN running the cached function should be retried
        assert mocks() == 1

    def test_no_retry(self, cache_file):
        # GIVEN a cached function and cache with retry off

        @cachalot.Cache(path=cache_file, size=1)
        def mocks():
            return 1

        # WHEN an empty result is cached
        mocks()
        cache = cachalot.Cache(path=cache_file, size=2,
                               retry=True)
        cache.db.update({'value': '0'})

        # THEN running the cached function should return the empty result
        assert mocks() == 0

    def test_no_renew_on_read(self, cache_file):
        # GIVEN a cached function and cache with renew on read off

        @cachalot.Cache(path=cache_file, size=1, renew_on_read=False)
        def mocks():
            return 1
        mocks()

        # WHEN the result is read from cache
        cache = cachalot.Cache(path=cache_file, size=1, renew_on_read=False)
        time_before = cache.db.all()[0]['time']
        mocks()
        time_after = cache.db.all()[0]['time']

        # THEN the time should not change
        assert time_before == time_after

    def test_method(self, cache_file):
        # GIVEN a cached method

        class Mock():

            @cachalot.Cache(path=cache_file, size=10)
            def mocks(self):
                return 1

        # WHEN running it
        mock = Mock()
        mock.mocks()
        mock.mocks()

        # THEN it should get cached
        cache = cachalot.Cache(path=cache_file, size=2,
                               retry=True)
        assert len(cache) == 1
